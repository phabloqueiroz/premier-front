import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {MenuModule} from './menu/menu.module';
import {OperadorasModule} from './operadoras/operadoras.module';
import {ProdutosModule} from './produtos/produtos.module';
import {TabelasPrecosModule} from './tabelas-precos/tabelas-precos.module';
import {MAT_DATE_LOCALE, MatDatepickerModule, MatDividerModule, MatNativeDateModule, MatToolbarModule} from '@angular/material';
import {VendedoresModule} from './vendedores/vendedores.module';
import {VendasModule} from './vendas/vendas.module';
import {RelatoriosModule} from './relatorios/relatorios.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MenuModule,
    MatToolbarModule,
    MatDividerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    OperadorasModule,
    ProdutosModule,
    TabelasPrecosModule,
    VendedoresModule,
    VendasModule,
    RelatoriosModule
  ],
  providers: [
    HttpClientModule,
    {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
