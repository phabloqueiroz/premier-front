import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperadorasFormModule } from './operadoras-view/operadoras-form/operadoras-form.module';
import { OperadorasDashboardModule } from './operadoras-view/operadoras-dashboard/operadoras-dashboard.module';

@NgModule({
  imports: [
    CommonModule,
    OperadorasFormModule,
    OperadorasDashboardModule
  ],
  declarations: []
})
export class OperadorasModule { }
