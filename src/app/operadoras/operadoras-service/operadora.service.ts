import {Operadora} from './../operadoras-model/operadora';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ApiService} from '../../uteis/uteis-service/api.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OperadoraService {

  public post(operadora: Operadora): Observable<any> {
    return this._apiService.post('operadoras', JSON.parse(JSON.stringify(operadora)));
  }

  public getById(id: number): Observable<Operadora> {
    return this._apiService.getById('operadoras', id);
  }

  public get(): Observable<Operadora[]> {
    return this._apiService.get('operadoras');
  }

  public getActive(): Observable<Operadora[]> {
    return this._apiService.get('operadoras', {params: new HttpParams().set('somenteAtivas', 'true')});
  }

  public put(operadora: Operadora): Observable<any> {
    return this._apiService.put('operadoras', JSON.parse(JSON.stringify(operadora)));
  }

  public edit(id: number) {
    this._router.navigate(['/operadoras', id]);
  }

  public dashboard() {
    this._router.navigate(['/operadoras']);
  }

  public novo() {
    this._router.navigate(['/operadoras/novo']);
  }

  constructor(
    private _apiService: ApiService,
    private _router: Router
  ) {
  }
}
