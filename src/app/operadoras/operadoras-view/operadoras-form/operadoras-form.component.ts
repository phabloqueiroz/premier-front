import {FormBuilder, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Operadora} from './../../operadoras-model/operadora';
import {OperadoraService} from './../../operadoras-service/operadora.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-operadoras-form',
  templateUrl: './operadoras-form.component.html',
  styleUrls: ['./operadoras-form.component.css']
})
export class OperadorasFormComponent implements OnInit {

  id: any;

  operadoraForm = this._formBuilder.group({
    nome: ['', [Validators.required]],
    ativo: [true]
  });

  constructor(
    private _formBuilder: FormBuilder,
    private _operadoraService: OperadoraService,
    private _route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.id = this._route.snapshot.paramMap.get('id');
    if (this.id) {
      this._operadoraService.getById(this.id).subscribe(
        data => this.operadoraForm.setValue({
          nome: data.nome,
          ativo: data.ativo
        }),
        err => {
          console.log(err);
          this.snackBar.open('Erro ao carregar dados.', 'Fechar', {duration: 5000,});
        }
      );

    }
  }

  onSubmit() {
    if (this.operadoraForm.valid) {
      if (this.id) {
        const operadora: Operadora = this.operadoraForm.value;
        operadora.id = this.id;
        this._operadoraService.put(operadora).subscribe(
          () => {
            this.snackBar.open('Operadora atualizada com sucesso.', 'Fechar', {duration: 3000,});
          },
          err => {
            console.log(err);
            this.snackBar.open('Erro ao atualizar operadora.', 'Fechar', {duration: 5000,});
          },
          () => this._operadoraService.dashboard()
        );
      } else {
        this._operadoraService.post(this.operadoraForm.value).subscribe(
          () => {
            this.snackBar.open('Operadora salva com sucesso.', 'Fechar', {duration: 3000,});
          },
          err => {
            console.log(err);
            this.snackBar.open('Erro ao salvar operadora.', 'Fechar', {duration: 5000,});
          },
          () => this._operadoraService.dashboard()
        );
      }
    } else {
      this.operadoraForm.markAsTouched();
    }
  }

  cancelar() {
    this._operadoraService.dashboard();
  }
}
