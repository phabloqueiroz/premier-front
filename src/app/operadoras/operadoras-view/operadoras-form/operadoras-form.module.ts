import {ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OperadorasFormComponent} from './operadoras-form.component';
import {MatButtonModule, MatCheckboxModule, MatInputModule, MatSnackBarModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule
  ],
  declarations: [OperadorasFormComponent]
})
export class OperadorasFormModule {
}
