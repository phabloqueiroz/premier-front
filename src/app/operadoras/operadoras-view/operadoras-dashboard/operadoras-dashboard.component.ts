import {OperadoraService} from './../../operadoras-service/operadora.service';
import {Operadora} from './../../operadoras-model/operadora';
import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-operadoras-dashboard',
  templateUrl: './operadoras-dashboard.component.html',
  styleUrls: ['./operadoras-dashboard.component.css']
})
export class OperadorasDashboardComponent implements OnInit {

  columnsToDisplay = ['nome', 'ativo', 'editar'];

  dataSource: MatTableDataSource<Operadora>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _operadoraService: OperadoraService
  ) {
  }

  ngOnInit() {
    this._operadoraService.get().subscribe(
      dados => {
        this.dataSource = new MatTableDataSource(dados);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err => {
        console.log(err);
      }
    );
  }

  editar(id: number) {
    this._operadoraService.edit(id);
  }

  novo() {
    this._operadoraService.novo();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
