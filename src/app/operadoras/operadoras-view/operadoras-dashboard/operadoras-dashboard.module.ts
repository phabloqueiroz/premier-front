import {UteisModule} from './../../../uteis/uteis.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OperadorasDashboardComponent} from './operadoras-dashboard.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    UteisModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [OperadorasDashboardComponent]
})
export class OperadorasDashboardModule {
}
