import { Operadora } from './../../operadoras/operadoras-model/operadora';

export class Produto {
    id: number;
    operadora: Operadora;
    nome: string;
    ativo: boolean;
    vidas: number;
    acomodacao: string;
    obstetricia_inclusa: boolean;
    descricao: string;
}
