import {ProdutosService} from './../../produtos-service/produtos.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {Produto} from '../../produtos-model/produto';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-produtos-dashboard',
  templateUrl: './produtos-dashboard.component.html',
  styleUrls: ['./produtos-dashboard.component.css']
})
export class ProdutosDashboardComponent implements OnInit {

  columnsToDisplay = ['operadora.nome', 'descricao', 'ativo', 'editar'];
  dataSource: MatTableDataSource<Produto>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _produtoService: ProdutosService,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {

    this._produtoService.get().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err => {
        console.log(err);
        this.snackBar.open('Erro ao carregar produtos', 'Fechar', {duration: 5000});
      }
    );

  }

  editar(id: number) {
    this._produtoService.editar(id);
  }

  novo() {
    this._produtoService.novo();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
