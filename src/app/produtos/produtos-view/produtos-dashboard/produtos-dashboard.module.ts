import {UteisModule} from './../../../uteis/uteis.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProdutosDashboardComponent} from './produtos-dashboard.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    UteisModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule
  ],
  declarations: [ProdutosDashboardComponent]
})
export class ProdutosDashboardModule {
}
