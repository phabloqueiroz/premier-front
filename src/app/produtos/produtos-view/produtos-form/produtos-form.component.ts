import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';

import {Operadora} from './../../../operadoras/operadoras-model/operadora';
import {ProdutosService} from './../../produtos-service/produtos.service';
import {OperadoraService} from 'src/app/operadoras/operadoras-service/operadora.service';
import {Produto} from '../../produtos-model/produto';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-produtos-form',
  templateUrl: './produtos-form.component.html',
  styleUrls: ['./produtos-form.component.css']
})
export class ProdutosFormComponent implements OnInit {

  operadoras: Operadora[];
  acomodacoes: any[];

  id: any;

  produtoForm = this._formBuilder.group({
    operadora: this._formBuilder.group({
      id: [null, Validators.required]
    }),
    nome: ['', [Validators.required]],
    ativo: [true],
    vidas: [1, [Validators.required, Validators.min(1)]],
    acomodacao: [null, [Validators.required]],
    obstetricia_inclusa: [false, Validators.required]
  });


  constructor(
    private _formBuilder: FormBuilder,
    private _produtoService: ProdutosService,
    private _operadoraSerive: OperadoraService,
    private _route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this._operadoraSerive.getActive().subscribe(
      data => this.operadoras = data,
      err => console.log(err)
    );

    this.acomodacoes = [
      {value: 'ENFERMARIA', name: 'Enfermaria'},
      {value: 'APARTAMENTO', name: 'Apartamento'}
    ];

    this.id = this._route.snapshot.paramMap.get('id');

    if (this.id) {
      this._produtoService.getById(this.id).subscribe(
        data => this.produtoForm.patchValue(data),
        err => {
          this.snackBar.open('Erro ao carregar produto.', 'Fechar', {duration: 5000,});
          console.log(err);
        }
      );
    }
  }


  onSubmit() {
    if (this.produtoForm.valid) {
      if (this.id) {
        let produto: Produto = this.produtoForm.value;
        produto.id = this.id;
        this._produtoService.put(produto).subscribe(
          () => {
            this.snackBar.open('Produto atualizado com sucesso.', 'Fechar', {duration: 3000,});
          },
          err => {
            console.log(err);
            this.snackBar.open('Erro ao atualizar produto.', 'Fechar', {duration: 5000,});
          },
          () => this._produtoService.dashboard()
        );
      } else {
        this._produtoService.post(this.produtoForm.value).subscribe(
          () => {
            this.snackBar.open('Produto salvo com sucesso.', 'Fechar', {duration: 3000,});
          },
          err => {
            this.snackBar.open('Erro ao salvar produto.', 'Fechar', {duration: 5000,});
            console.log(err);
          },
          () => this._produtoService.dashboard()
        );
      }

    } else {
      this.produtoForm.markAsTouched();
    }
  }

  cancelar() {
    this._produtoService.dashboard();
  }
}
