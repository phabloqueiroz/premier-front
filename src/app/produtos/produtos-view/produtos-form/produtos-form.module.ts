import {ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProdutosFormComponent} from './produtos-form.component';
import {MatButtonModule, MatCheckboxModule, MatInputModule, MatRadioModule, MatSelectModule, MatSnackBarModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatRadioModule,
    MatSelectModule
  ],
  declarations: [ProdutosFormComponent]
})
export class ProdutosFormModule {
}
