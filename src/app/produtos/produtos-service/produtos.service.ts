import {Router} from '@angular/router';
import {ApiService} from './../../uteis/uteis-service/api.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Produto} from '../produtos-model/produto';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  constructor(
    private _apiService: ApiService,
    private _router: Router
  ) {
  }

  public post(produto: Produto): Observable<any> {
    return this._apiService.post('produtos', JSON.parse(JSON.stringify(produto)));
  }

  public getById(id: number): Observable<Produto> {
    return this._apiService.getById('produtos', id);
  }

  public get(): Observable<Produto[]> {
    return this._apiService.get('produtos');
  }

  public put(produto: Produto): Observable<any> {
    return this._apiService.put('produtos', JSON.parse(JSON.stringify(produto)));
  }


  public getPorOperadora(operadoraId: number, somenteComTabelaPrecoAtiva: boolean = false): Observable<Produto[]> {
    let options = {};
    if (somenteComTabelaPrecoAtiva) {
      options = {params: new HttpParams().set('somenteComTabelaPrecoAtiva', 'true')};
    }
    return this._apiService.get(`operadoras/${operadoraId}/produtos`, options);
  }

  getAtivos(): Observable<Produto[]> {
    return this._apiService.get('produtos', {params: new HttpParams().set('somenteAtivos', 'true')});
  }


  public editar(id: number) {
    this._router.navigate(['/produtos', id]);
  }

  public dashboard() {
    this._router.navigate(['/produtos']);
  }

  public novo() {
    this._router.navigate(['/produtos/novo']);
  }


}
