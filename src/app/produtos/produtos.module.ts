import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProdutosDashboardModule } from './produtos-view/produtos-dashboard/produtos-dashboard.module';
import { ProdutosFormModule } from './produtos-view/produtos-form/produtos-form.module';

@NgModule({
  imports: [
    CommonModule,
    ProdutosDashboardModule,
    ProdutosFormModule
  ]
})
export class ProdutosModule { }
