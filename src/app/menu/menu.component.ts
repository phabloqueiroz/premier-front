import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/components/common/menuitem';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];

  constructor(private _router: Router) {
  }

  ngOnInit() {
    this.items = [
      {
        label: 'Manutenções',
        items: [
          {label: 'Operadoras', routerLink: 'operadoras'},
          {label: 'Produtos', routerLink: 'produtos'},
          {label: 'Tabelas de preço', routerLink: 'tabelas-precos'},
        ]
      },
      {
        label: 'Vendas'
      }
    ];
  }

  irPara(rota: string[]) {
    this._router.navigate(rota);
  }

}
