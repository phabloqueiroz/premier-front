import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RelatoriosVendasModule} from './relatorios-view/relatorios-vendas/relatorios-vendas.module';
import {RelatoriosVendedorModule} from './relatorios-view/relatorios-vendedor/relatorios-vendedor.module';

@NgModule({
  imports: [
    CommonModule,
    RelatoriosVendasModule,
    RelatoriosVendedorModule
  ],
})
export class RelatoriosModule {
}
