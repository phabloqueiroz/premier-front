import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RelatoriosVendasComponent} from './relatorios-vendas.component';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTableModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  declarations: [RelatoriosVendasComponent]
})
export class RelatoriosVendasModule {
}
