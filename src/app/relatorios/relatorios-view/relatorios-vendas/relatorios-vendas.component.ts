import {Component, OnInit, ViewChild} from '@angular/core';
import {RelatorioService} from '../../relatorios-service/relatorio-service';
import {MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ResultVendasPorOperadora} from '../../relatorios-model/result-vendas-por-operadora';
import {FormBuilder, Validators} from '@angular/forms';
import {ResultVendasTotais} from '../../relatorios-model/result-vendas-totais';

@Component({
  selector: 'app-tabelas-precos-dashboard',
  templateUrl: './relatorios-vendas.component.html',
  styleUrls: ['./relatorios-vendas.component.css']
})
export class RelatoriosVendasComponent implements OnInit {

  vendasPorOperadoraColumnsToDisplay = ['operadora', 'vendedor', 'total_vendas', 'valor_total'];
  vendasTotaisColumnsToDisplay = ['vendedor', 'total_vendas', 'valor_total'];
  relatorioVendaForm: any;

  @ViewChild(MatSort) sort: MatSort;

  dataSourceVendasPorOperadora: MatTableDataSource<ResultVendasPorOperadora> | null;
  dataSourceVendasTotais: MatTableDataSource<ResultVendasTotais> | null;

  hoje = new Date();
  data_inicio = new Date();

  constructor(
    private _relatorioService: RelatorioService,
    public snackBar: MatSnackBar,
    private _formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.data_inicio.setDate(1);

    this.relatorioVendaForm = this._formBuilder.group({
      data_inicio: [this.data_inicio, Validators.required],
      data_fim: [this.hoje, Validators.required]
    });
  }

  atualizaDataSource(dataVendasPorOperadora: any, dataVendasTotais) {
    this.dataSourceVendasPorOperadora = new MatTableDataSource(dataVendasPorOperadora);
    this.dataSourceVendasPorOperadora.sort = this.sort;

    this.dataSourceVendasTotais = new MatTableDataSource(dataVendasTotais);
    this.dataSourceVendasTotais.sort = this.sort;
  }

  consultar() {
    if (this.relatorioVendaForm.invalid) {
      this.snackBar.open('Favor informar as datas de início e fim.', 'Fechar', {duration: 5000,});

    } else if (this.relatorioVendaForm.get('data_fim').value < this.relatorioVendaForm.get('data_inicio').value) {
      this.snackBar.open('A data de fim deve ser maior que a data de início.', 'Fechar', {duration: 5000,});

    } else {
      this._relatorioService.getVendasPorOperadora(this.relatorioVendaForm.value).subscribe(
        data => {
          this._relatorioService.getVendasTotais(this.relatorioVendaForm.value).subscribe(
            data1 => {
              this.atualizaDataSource(data, data1);
            },
            error1 => {
              console.log(error1);
              this.snackBar.open('Erro ao carregar dados.', 'Fechar', {duration: 5000,});
            });
        }, error1 => {
          console.log(error1);
          this.snackBar.open('Erro ao carregar dados.', 'Fechar', {duration: 5000,});
        });
    }
  }

}
