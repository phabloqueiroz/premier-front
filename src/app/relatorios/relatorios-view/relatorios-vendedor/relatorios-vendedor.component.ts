import {Component, OnInit} from '@angular/core';
import {RelatorioService} from '../../relatorios-service/relatorio-service';
import {MatSnackBar} from '@angular/material';
import {FormBuilder, Validators} from '@angular/forms';
import {Vendedor} from '../../../vendedores/vendedores-model/vendedor';
import {VendedoresService} from '../../../vendedores/vendedores-service/vendedores.service';
import {saveAs} from 'file-saver';


@Component({
  selector: 'app-tabelas-precos-dashboard',
  templateUrl: './relatorios-vendedor.component.html',
  styleUrls: ['./relatorios-vendedor.component.css']
})
export class RelatoriosVendedorComponent implements OnInit {

  relatorioVendedorForm = this._formBuilder.group({
    data_inicio: [null, Validators.required],
    data_fim: [null, Validators.required],
    vendedor: this._formBuilder.group({
      id: [null, Validators.required]
    })
  });

  hoje = new Date();
  data_inicio = new Date();
  vendedores: Vendedor[];

  constructor(
    private _relatorioService: RelatorioService,
    public snackBar: MatSnackBar,
    private _formBuilder: FormBuilder,
    private _vendedorService: VendedoresService,
  ) {
  }

  ngOnInit() {
    this.data_inicio.setDate(1);

    this.relatorioVendedorForm.patchValue({
        data_inicio: this.data_inicio,
        data_fim: this.hoje
      }
    );

    this._vendedorService.getAtivos().subscribe(
      data => this.vendedores = data,
      error1 => {
        console.log(error1);
        this.snackBar.open('Erro ao carregar vendedores.', 'Fechar', {duration: 5000,});
      }
    );
  }

  consultar() {
    if (this.relatorioVendedorForm.invalid) {
      this.snackBar.open('Favor informar as datas de início, fim e selecione um vendedor.', 'Fechar', {duration: 5000,});

    } else if (this.relatorioVendedorForm.get('data_fim').value < this.relatorioVendedorForm.get('data_inicio').value) {
      this.snackBar.open('A data de fim deve ser maior que a data de início.', 'Fechar', {duration: 5000,});

    } else {
      this._relatorioService.getVendasPorVendedor(this.relatorioVendedorForm.value).subscribe(
        data => {
          const vendedor = this.vendedores.filter(vend => vend.id === this.relatorioVendedorForm.get('vendedor.id').value).pop();
          saveAs(data, `Relatorio de vendas - ${vendedor.pessoa_fisica.nome}.xlsx`);
        }, error1 => {
          console.log(error1);
          this.snackBar.open('Erro ao carregar dados.', 'Fechar', {duration: 5000,});
        });
    }

  }

}
