export class ResultVendasTotais {
  total_vendas: number;
  valor_total: number;
  vendedor: string;
}
