export class ResultVendasPorOperadora {
  operadora: string;
  total_vendas: number;
  valor_total: number;
  vendedor: string;
}
