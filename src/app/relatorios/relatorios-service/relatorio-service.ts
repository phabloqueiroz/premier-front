import {Injectable} from '@angular/core';
import {ApiService} from '../../uteis/uteis-service/api.service';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResultVendasPorOperadora} from '../relatorios-model/result-vendas-por-operadora';
import {ResultVendasTotais} from '../relatorios-model/result-vendas-totais';

@Injectable({
  providedIn: 'root'
})
export class RelatorioService {

  constructor(
    private _apiService: ApiService,
  ) {
  }

  getVendasPorOperadora(relatorioVendas: any): Observable<ResultVendasPorOperadora[]> {
    let params = new HttpParams();
    params = params.append('data_inicio', relatorioVendas.data_inicio.toISOString().slice(0, 10));
    params = params.append('data_fim', relatorioVendas.data_fim.toISOString().slice(0, 10));
    return this._apiService.get('relatorios/vendas-por-operadora', {params: params});
  }

  getVendasTotais(relatorioVendas: any): Observable<ResultVendasTotais[]> {
    let params = new HttpParams();
    params = params.append('data_inicio', relatorioVendas.data_inicio.toISOString().slice(0, 10));
    params = params.append('data_fim', relatorioVendas.data_fim.toISOString().slice(0, 10));
    return this._apiService.get('relatorios/vendas-totais', {params: params});
  }

  getVendasPorVendedor(relatorioVendas: any): Observable<Blob> {
    const data_inicio = relatorioVendas.data_inicio.toISOString().slice(0, 10);
    const data_fim = relatorioVendas.data_fim.toISOString().slice(0, 10);
    return this._apiService.getFile(
      `relatorios/vendas/vendedor/${relatorioVendas.vendedor.id}?data_inicio=${data_inicio}&data_fim=${data_fim}`);

  }


}
