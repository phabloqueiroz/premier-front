import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _api = 'http://localhost:5000';

  constructor(
    private _http: HttpClient) {
  }

  public get(sufixo: string, options?: any): Observable<any> {
    return this._http.get(`${this._api}/${sufixo}`, options);
  }

  public getById(sufixo: string, id: number): Observable<any> {
    return this.get(`${sufixo}/${id}`);
  }

  public post(sufixo: string, json: JSON): Observable<any> {
    return this._http.post(`${this._api}/${sufixo}`, json);
  }

  public put(sufixo: string, json: JSON): Observable<any> {
    return this._http.put(`${this._api}/${sufixo}`, json);
  }

  public getFile(sufixo: string): Observable<Blob> {
    return this._http.get(`${this._api}/${sufixo}`, {responseType: 'blob'});
  }

}
