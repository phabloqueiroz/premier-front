import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UteisService {

  constructor() { }

  isInvalid(form: FormGroup, campo: string): boolean {
    return !form.get(campo).valid && (form.touched || form.get(campo).touched);
  }
}
