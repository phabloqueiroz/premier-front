import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormataValorPipe } from './uteis-pipe/formata-valor.pipe';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FormataValorPipe
  ],
  exports: [
    FormataValorPipe
  ]
})
export class UteisModule { }
