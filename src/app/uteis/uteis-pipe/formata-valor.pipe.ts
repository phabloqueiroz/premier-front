import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formataValor'
})
export class FormataValorPipe implements PipeTransform {

  transform(value: any, type: string): any {
    if(type == 'boolean'){
      return value ? 'Ativo' : 'Inativo';
    }
    return value;
  }

}
