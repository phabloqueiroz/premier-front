import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {OperadorasFormComponent} from './operadoras/operadoras-view/operadoras-form/operadoras-form.component';
import {OperadorasDashboardComponent} from './operadoras/operadoras-view/operadoras-dashboard/operadoras-dashboard.component';
import {ProdutosDashboardComponent} from './produtos/produtos-view/produtos-dashboard/produtos-dashboard.component';
import {ProdutosFormComponent} from './produtos/produtos-view/produtos-form/produtos-form.component';
import {TabelasPrecosFormComponent} from './tabelas-precos/tabelas-precos-view/tabelas-precos-form/tabelas-precos-form.component';
import {TabelasPrecosDashboardComponent} from './tabelas-precos/tabelas-precos-view/tabelas-precos-dashboard/tabelas-precos-dashboard.component';
import {VendedoresDashboardComponent} from './vendedores/vendedores-view/vendedores-dashboard/vendedores-dashboard.component';
import {VendedoresFormComponent} from './vendedores/vendedores-view/vendedores-form/vendedores-form.component';
import {VendasFormComponent} from './vendas/vendas-view/vendas-form/vendas-form.component';
import {VendasDashboardComponent} from './vendas/vendas-view/vendas-dashboard/vendas-dashboard.component';
import {RelatoriosVendasComponent} from './relatorios/relatorios-view/relatorios-vendas/relatorios-vendas.component';
import {RelatoriosVendedorComponent} from './relatorios/relatorios-view/relatorios-vendedor/relatorios-vendedor.component';

const routes: Routes = [
  {path: 'operadoras/novo', component: OperadorasFormComponent},
  {path: 'operadoras/:id', component: OperadorasFormComponent},
  {path: 'operadoras', component: OperadorasDashboardComponent},
  {path: 'produtos/novo', component: ProdutosFormComponent},
  {path: 'produtos/:id', component: ProdutosFormComponent},
  {path: 'produtos', component: ProdutosDashboardComponent},
  {path: 'produtos/:produtoId/tabelas-precos/novo', component: TabelasPrecosFormComponent},
  {path: 'tabelas-precos/:id', component: TabelasPrecosFormComponent},
  {path: 'tabelas-precos', component: TabelasPrecosDashboardComponent},
  {path: 'vendedores', component: VendedoresDashboardComponent},
  {path: 'vendedores/novo', component: VendedoresFormComponent},
  {path: 'vendedores/:id', component: VendedoresFormComponent},
  {path: 'vendas/novo', component: VendasFormComponent},
  {path: 'vendas/:adesao', component: VendasFormComponent},
  {path: 'vendas', component: VendasDashboardComponent},
  {path: 'relatorios/vendas', component: RelatoriosVendasComponent},
  {path: 'relatorios/vendedor', component: RelatoriosVendedorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
