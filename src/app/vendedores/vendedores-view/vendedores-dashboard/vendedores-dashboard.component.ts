import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Vendedor} from '../../vendedores-model/vendedor';
import {VendedoresService} from '../../vendedores-service/vendedores.service';

@Component({
  selector: 'app-vendedores-dashboard',
  templateUrl: './vendedores-dashboard.component.html',
  styleUrls: ['./vendedores-dashboard.component.css']
})
export class VendedoresDashboardComponent implements OnInit {

  columnsToDisplay = ['pessoa_fisica.nome', 'pessoa_fisica.data_nascimento', 'ativo', 'editar'];
  dataSource: MatTableDataSource<Vendedor>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _vendedoresService: VendedoresService,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this._vendedoresService.get().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error1 => {
        console.error(error1);
        this.snackBar.open('Erro ao carregar vendedores', 'Fechar', {duration: 5000});
      }
    );
  }

  editar(id: number) {
    this._vendedoresService.editar(id);
  }

  novo() {
    this._vendedoresService.novo();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
