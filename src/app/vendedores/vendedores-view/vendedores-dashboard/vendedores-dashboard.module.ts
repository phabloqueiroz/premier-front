import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendedoresDashboardComponent } from './vendedores-dashboard.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  declarations: [VendedoresDashboardComponent]
})
export class VendedoresDashboardModule { }
