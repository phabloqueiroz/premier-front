import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {VendedoresService} from '../../vendedores-service/vendedores.service';
import {ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {Produto} from '../../../produtos/produtos-model/produto';
import {Vendedor} from '../../vendedores-model/vendedor';

@Component({
  selector: 'app-vendedores-form',
  templateUrl: './vendedores-form.component.html',
  styleUrls: ['./vendedores-form.component.css']
})
export class VendedoresFormComponent implements OnInit {

  id;

  vendedorForm = this._formBuilder.group({
    pessoa_fisica: this._formBuilder.group({
      id: [],
      nome: ['', Validators.required],
      data_nascimento: [null, Validators.required],
      cpf: [null, [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      email: [null, Validators.email]
    }),
    ativo: [true, Validators.required]
  });

  constructor(
    private _formBuilder: FormBuilder,
    private _vendedoresService: VendedoresService,
    private _route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {

    this.id = this._route.snapshot.paramMap.get('id');
    if (this.id) {
      this._vendedoresService.getPorId(this.id).subscribe(
        data => this.vendedorForm.patchValue(data),
        error1 => {
          console.error(error1);
          this.snackBar.open('Erro ao carregar vendedor', 'Fechar', {duration: 5000});
        }
      );
    }
  }

  onSubmit() {
    console.log(this.vendedorForm);

    if (this.vendedorForm.valid) {
      if (this.id) {
        let vendedor: Vendedor = this.vendedorForm.value;
        vendedor.id = this.id;
        if (vendedor.pessoa_fisica.email === "") {
          vendedor.pessoa_fisica.email = null;
        }
        this._vendedoresService.put(vendedor).subscribe(
          () => {
            this.snackBar.open('Vendedor atualizado com sucesso.', 'Fechar', {duration: 3000,});
          },
          err => {
            console.log(err);
            this.snackBar.open('Erro ao atualizar vendedor.', 'Fechar', {duration: 5000,});
          },
          () => this._vendedoresService.dashboard()
        );
      } else {
        this._vendedoresService.post(this.vendedorForm.value).subscribe(
          () => {
            this.snackBar.open('Vendedor salvo com sucesso.', 'Fechar', {duration: 3000,});
          },
          err => {
            this.snackBar.open('Erro ao salvar vendedor.', 'Fechar', {duration: 5000,});
            console.log(err);
          },
          () => this._vendedoresService.dashboard()
        );
      }

    } else {
      this.vendedorForm.markAsTouched();
    }
  }

  cancelar() {
    this._vendedoresService.dashboard();
  }
}
