import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendedoresFormComponent } from './vendedores-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatInputModule, MatSnackBarModule} from '@angular/material';
import {NgxMaskModule} from 'ngx-mask'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatDatepickerModule,
    NgxMaskModule.forRoot({})
  ],
  declarations: [VendedoresFormComponent],
  exports: [VendedoresFormComponent]
})
export class VendedoresFormModule { }
