import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VendedoresDashboardModule} from './vendedores-view/vendedores-dashboard/vendedores-dashboard.module';
import {VendedoresFormModule} from './vendedores-view/vendedores-form/vendedores-form.module';

@NgModule({
  imports: [
    CommonModule,
    VendedoresDashboardModule,
    VendedoresFormModule
  ],
  declarations: []
})
export class VendedoresModule { }
