import {PessoaFisica} from './pessoa-fisica';

export class Vendedor {
  id: number;
  ativo: boolean;
  pessoa_fisica: PessoaFisica;
}
