import {Telefone} from './telefone';

export class PessoaFisica {
  id: number;
  nome: string;
  email: string;
  data_nascimento: Date;
  cpf: string;
  telefones: Telefone[];
}
