import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Vendedor} from '../vendedores-model/vendedor';
import {ApiService} from '../../uteis/uteis-service/api.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VendedoresService {

  private _sufixo = 'vendedores';

  constructor(
    private _apiService: ApiService,
    private _router: Router
  ) {
  }

  getPorId(id: number): Observable<Vendedor> {
    return this._apiService.getById(this._sufixo, id);
  }

  get(): Observable<Vendedor[]> {
    return this._apiService.get(this._sufixo);
  }

  getAtivos(): Observable<Vendedor[]> {
    return this._apiService.get(this._sufixo, {params: new HttpParams().set('somenteAtivos', 'true')});
  }

  put(vendedor: Vendedor): Observable<any> {
    return this._apiService.put(this._sufixo, JSON.parse(JSON.stringify(vendedor)));
  }

  post(vendedor: Vendedor): Observable<any> {
    return this._apiService.post(this._sufixo, JSON.parse(JSON.stringify(vendedor)));
  }

  dashboard() {
    this._router.navigate(['vendedores']);
  }

  novo() {
    this._router.navigate(['vendedores', 'novo']);
  }

  editar(id: number) {
    this._router.navigate(['vendedores', id]);
  }

}
