import {Component, OnInit, ViewChild} from '@angular/core';
import {Operadora} from '../../../operadoras/operadoras-model/operadora';
import {Produto} from '../../../produtos/produtos-model/produto';
import {OperadoraService} from '../../../operadoras/operadoras-service/operadora.service';
import {ProdutosService} from '../../../produtos/produtos-service/produtos.service';
import {TabelaPrecos} from '../../tabelas-precos-model/tabela-precos';
import {TabelasPrecosService} from '../../tabelas-precos-service/tabelas-precos.service';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-tabelas-precos-dashboard',
  templateUrl: './tabelas-precos-dashboard.component.html',
  styleUrls: ['./tabelas-precos-dashboard.component.css']
})
export class TabelasPrecosDashboardComponent implements OnInit {

  operadoras: Operadora[];
  produtos: Produto[];
  produtoSelecionado: Produto;

  columnsToDisplay = ['descricao', 'data_criacao', 'ativa', 'editar'];

  dataSource: MatTableDataSource<TabelaPrecos> | null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _tabelaPrecosService: TabelasPrecosService,
    private _produtoService: ProdutosService,
    private _operadoraService: OperadoraService,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this._operadoraService.getActive().subscribe(
      data => this.operadoras = data,
      error1 => {
        console.log(error1);
        this.snackBar.open('Erro ao carregar operadoras.', 'Fechar', {duration: 5000,});
      }
    );
  }

  atualizaDataSource(data: any) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  carregarProdutos(operadora: Operadora) {
    this.atualizaDataSource([]);
    this._produtoService.getPorOperadora(operadora.id).subscribe(
      data => {
        this.produtos = data;
      },
      error1 => {
        console.log(error1);
        this.snackBar.open('Erro ao carregar produtos da operadora.', 'Fechar', {duration: 5000,});
      }
    );
  }

  carregarTabelasPrecos(produto: Produto) {
    this.produtoSelecionado = produto;
    this._tabelaPrecosService.getPorProduto(produto.id).subscribe(
      data => {
        this.atualizaDataSource(data);
      },
      error1 => {
        console.log(error1);
        this.snackBar.open('Erro ao carregar tabelas de preço do produto.', 'Fechar', {duration: 5000,});
      }
    );
  }

  novo() {
    this._tabelaPrecosService.novo(this.produtoSelecionado.id);
  }

  editar(id: number) {
    this._tabelaPrecosService.editar(id);
  }
}
