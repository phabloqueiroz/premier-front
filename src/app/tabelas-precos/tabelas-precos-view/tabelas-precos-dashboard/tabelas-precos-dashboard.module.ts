import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabelasPrecosDashboardComponent} from './tabelas-precos-dashboard.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTableModule
} from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSnackBarModule
  ],
  declarations: [TabelasPrecosDashboardComponent]
})
export class TabelasPrecosDashboardModule {
}
