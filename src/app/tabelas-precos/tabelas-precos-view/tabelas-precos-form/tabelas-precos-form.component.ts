import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {UteisService} from '../../../uteis/uteis-service/uteis.service';
import {TabelasPrecosService} from '../../tabelas-precos-service/tabelas-precos.service';
import {Produto} from '../../../produtos/produtos-model/produto';
import {ProdutosService} from '../../../produtos/produtos-service/produtos.service';
import {ActivatedRoute} from '@angular/router';
import {TabelaPrecos} from '../../tabelas-precos-model/tabela-precos';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-tabelas-precos-form',
  templateUrl: './tabelas-precos-form.component.html',
  styleUrls: ['./tabelas-precos-form.component.css']
})
export class TabelasPrecosFormComponent implements OnInit {

  tabelaPrecosForm = this._formBuilder.group({
    descricao: ['', Validators.required],
    produto: this._formBuilder.group({
      id: [null, Validators.required]
    }),
    ativa: [true, Validators.required],
    precos: this._formBuilder.array([])
  });

  produto: Produto;
  faixasEtarias: FormArray;
  id;

  constructor(
    private _formBuilder: FormBuilder,
    private _uteisService: UteisService,
    private _tabelaPrecosService: TabelasPrecosService,
    private _produtoService: ProdutosService,
    private _route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this._tabelaPrecosService.getFaixasEtarias().subscribe(
      data => {
        this.faixasEtarias = this.tabelaPrecosForm.get('precos') as FormArray;
        data.map(
          f =>
            this.faixasEtarias.push(
              this._formBuilder.group({
                faixa_etaria: this._formBuilder.group({
                  id: f.id,
                  descricao: f.descricao
                }),
                preco: [0, [Validators.required, Validators.min(0.01)]]
              })));
      },
      error2 => console.log(error2));

    const produtoId = +this._route.snapshot.paramMap.get('produtoId');
    this.tabelaPrecosForm.get('produto.id').setValue(produtoId);
    this._produtoService.getById(produtoId).subscribe(
      data => this.produto = data,
      error1 => console.log(error1)
    );

    this.id = this._route.snapshot.paramMap.get('id');
    if (this.id) {
      this._tabelaPrecosService.getPorId(this.id).subscribe(
        data => {
          this.tabelaPrecosForm.patchValue(data);
          this.produto = data.produto;
        },
        error1 => console.log(error1)
      );
    }
  }

  onSubmit() {
    if (this.tabelaPrecosForm.valid) {
      if (this.id) {
        const tabelaPrecos: TabelaPrecos = this.tabelaPrecosForm.value;
        tabelaPrecos.id = this.id;
        this._tabelaPrecosService.put(tabelaPrecos).subscribe(
          () => {
            this.snackBar.open('Tabela de preços atualizada com sucesso.', 'Fechar', {duration: 3000,});
          },
          error1 => {
            console.log(error1);
            this.snackBar.open('Erro ao atualizar tabela de preços.', 'Fechar', {duration: 5000,});
          },
          () => this._tabelaPrecosService.dashboard()
        );
      } else {
        this._tabelaPrecosService.post(this.tabelaPrecosForm.value).subscribe(
          () => {
            this.snackBar.open('Tabela de preços salva com sucesso.', 'Fechar', {duration: 3000,});
          },
          error1 => {
            console.log(error1);
            this.snackBar.open('Erro ao salvar tabela de preços.', 'Fechar', {duration: 5000,});
          },
          () => this._tabelaPrecosService.dashboard()
        );
      }
    } else {
      this.tabelaPrecosForm.markAsTouched();
    }
  }

  public cancelar() {
    this._tabelaPrecosService.dashboard();
  }

}
