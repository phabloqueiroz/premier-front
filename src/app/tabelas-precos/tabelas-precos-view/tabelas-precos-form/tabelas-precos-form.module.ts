import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabelasPrecosFormComponent} from './tabelas-precos-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatInputModule, MatListModule} from '@angular/material';
import {NgxCurrencyModule} from 'ngx-currency';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatListModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    NgxCurrencyModule,
    MatCheckboxModule,
    MatSnackBarModule
  ],
  declarations: [TabelasPrecosFormComponent]
})
export class TabelasPrecosFormModule {
}
