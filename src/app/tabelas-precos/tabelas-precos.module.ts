import {TabelasPrecosFormModule} from './tabelas-precos-view/tabelas-precos-form/tabelas-precos-form.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabelasPrecosDashboardModule} from './tabelas-precos-view/tabelas-precos-dashboard/tabelas-precos-dashboard.module';

@NgModule({
  imports: [
    CommonModule,
    TabelasPrecosFormModule,
    TabelasPrecosDashboardModule
  ]
})
export class TabelasPrecosModule {
}
