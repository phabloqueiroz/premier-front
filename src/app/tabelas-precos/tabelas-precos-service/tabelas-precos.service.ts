import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {FaixaEtaria} from '../tabelas-precos-model/faixa-etaria';
import {ApiService} from '../../uteis/uteis-service/api.service';
import {TabelaPrecos} from '../tabelas-precos-model/tabela-precos';

@Injectable({
  providedIn: 'root'
})
export class TabelasPrecosService {

  constructor(
    private _apiService: ApiService,
    private _router: Router
  ) {
  }

  public getFaixasEtarias(): Observable<FaixaEtaria[]> {
    return this._apiService.get('faixas-etarias');
  }

  public post(tabelaPrecos: TabelaPrecos): Observable<any> {
    return this._apiService.post('tabelas-precos', JSON.parse(JSON.stringify(tabelaPrecos)));
  }

  public put(tabelaPrecos: TabelaPrecos): Observable<any> {
    return this._apiService.put('tabelas-precos', JSON.parse(JSON.stringify(tabelaPrecos)));
  }

  public getPorProduto(idProduto: number): Observable<TabelaPrecos[]> {
    return this._apiService.get(`produtos/${idProduto}/tabelas-precos`);
  }

  public getPorId(id: number): Observable<TabelaPrecos> {
    return this._apiService.getById('tabelas-precos', id);
  }

  public novo(produtoId: number) {
    this._router.navigate(['produtos', produtoId, 'tabelas-precos', 'novo']);
  }

  public dashboard() {
    this._router.navigate(['/tabelas-precos']);
  }

  public editar(id: number) {
    this._router.navigate(['/tabelas-precos', id]);
  }
}
