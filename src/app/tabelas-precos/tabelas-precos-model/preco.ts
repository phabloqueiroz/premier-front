import { FaixaEtaria } from './faixa-etaria';

export class Preco {
    id: number;
    preco: number;
    faixa_etaria: FaixaEtaria;
}
