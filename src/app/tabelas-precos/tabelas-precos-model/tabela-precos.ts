import { Preco } from './preco';
import { Produto } from "src/app/produtos/produtos-model/produto";

export class TabelaPrecos {
    id: number;
    descricao: string;
    produto: Produto;
    precos: Preco[];
    data_criacao: Date;
    ativa: boolean;
}
