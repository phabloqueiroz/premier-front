export class FaixaEtaria {
    id: number;
    idade_minima: number;
    idade_maxima: number;
    descricao: string;
}
