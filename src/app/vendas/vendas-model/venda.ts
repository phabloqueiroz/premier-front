import {Vendedor} from '../../vendedores/vendedores-model/vendedor';
import {Beneficiario} from './beneficiario';
import {Produto} from '../../produtos/produtos-model/produto';
import {PessoaFisica} from '../../vendedores/vendedores-model/pessoa-fisica';

export class Venda {
  id: number;
  adesao: number;
  data: Date;
  vendedor: Vendedor;
  produto: Produto;
  beneficiarios: Beneficiario[];
  responsavel: PessoaFisica;
  taxa_adesao: number;
  status: string;
  comissionada: boolean;
  observacao: string;
}

export interface VendaMin {
  adesao: number;
  data: Date;
  operadora: string;
  produto: string;
  titular: string;
  valor: number;
  vidas: number;
}
