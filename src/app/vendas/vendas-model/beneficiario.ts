export class Beneficiario {
  id: number;
  nome: string;
  data_nascimento: Date;
  cpf: string;
  preco: number;
  e_titular: boolean;
}
