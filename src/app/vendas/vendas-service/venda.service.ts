import {Injectable} from '@angular/core';
import {ApiService} from '../../uteis/uteis-service/api.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Venda, VendaMin} from '../vendas-model/venda';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VendaService {

  private _sufixo = 'vendas';

  constructor(
    private _apiService: ApiService,
    private _router: Router
  ) {
  }

  formataData(d: Date): string {
    return d.toISOString().slice(0, 10);
  }

  get(buscaVenda: any): Observable<VendaMin[]> {
    let params = new HttpParams();
    if (buscaVenda.adesao) {
      return this._apiService.get(`${this._sufixo}/${buscaVenda.adesao}`);
    } else {
      if (buscaVenda.operadora_id) {
        params = params.append('operadora_id', buscaVenda.operadora_id);
      }
      if (buscaVenda.vendedor_id) {
        params = params.append('vendedor_id', buscaVenda.vendedor_id);
      }
      if (buscaVenda.data_inicio) {
        params = params.append('data_inicio', this.formataData(buscaVenda.data_inicio));
      }
      if (buscaVenda.data_fim) {
        params = params.append('data_fim', this.formataData(buscaVenda.data_fim));
      }
      if (buscaVenda.adesao) {
        params = params.append('adesao', buscaVenda.adesao);
      }
      return this._apiService.get(this._sufixo, {params: params});
    }

  }

  getById(id: number): Observable<Venda> {
    return this._apiService.get(`${this._sufixo}/adesao/${id}`);
  }

  post(venda: any): Observable<any> {
    return this._apiService.post(this._sufixo, JSON.parse(JSON.stringify(venda)));
  }

  put(venda: any): Observable<any> {
    return this._apiService.put(this._sufixo, JSON.parse(JSON.stringify(venda)));
  }

  getPreco(produtoId: number, idade: number) {
    return this._apiService.get(`produtos/${produtoId}/idade/${idade}`);
  }

  dashboard() {
    this._router.navigate(['vendas']);
  }

  novo() {
    this._router.navigate(['vendas', 'novo']);
  }

  editar(id: number) {
    this._router.navigate(['vendas', id]);
  }


}
