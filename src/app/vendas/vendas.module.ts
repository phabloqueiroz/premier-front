import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VendasDashboardModule} from './vendas-view/vendas-dashboard/vendas-dashboard.module';
import {VendasFormModule} from './vendas-view/vendas-form/vendas-form.module';

@NgModule({
  imports: [
    CommonModule,
    VendasDashboardModule,
    VendasFormModule
  ],
  declarations: []
})
export class VendasModule { }
