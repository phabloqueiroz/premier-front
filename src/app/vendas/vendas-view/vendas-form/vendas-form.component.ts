import {Component, OnInit} from '@angular/core';
import {Operadora} from '../../../operadoras/operadoras-model/operadora';
import {Produto} from '../../../produtos/produtos-model/produto';
import {ProdutosService} from '../../../produtos/produtos-service/produtos.service';
import {OperadoraService} from '../../../operadoras/operadoras-service/operadora.service';
import {VendedoresService} from '../../../vendedores/vendedores-service/vendedores.service';
import {VendaService} from '../../vendas-service/venda.service';
import {MatSnackBar} from '@angular/material';
import {Vendedor} from '../../../vendedores/vendedores-model/vendedor';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Venda} from '../../vendas-model/venda';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-vendas-form',
  templateUrl: './vendas-form.component.html',
  styleUrls: ['./vendas-form.component.css']
})
export class VendasFormComponent implements OnInit {

  adesao: any;
  vendaSelecionada: Venda;

  operadoras: Operadora[];
  produtos: Produto[];
  produtoSelecionado: Produto;
  vendedores: Vendedor[];

  vidas = 1;
  adicionarDependenteHabilitado = false;
  removerDependenteHabilitado = false;

  hoje = new Date();

  vendaForm = this._formBuilder.group({
    adesao: [null, Validators.required],
    data: [this.hoje, Validators.required],
    operadora: this._formBuilder.group({
      id: [null, Validators.required]
    }),
    produto: this._formBuilder.group({
      id: [null, Validators.required],
    }),
    vendedor: this._formBuilder.group({
      id: [null, Validators.required]
    }),
    observacao: [null],
    responsavel: this._formBuilder.group({
      id: [null],
      nome: [null, Validators.required],
      cpf: [null, [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      telefones: this._formBuilder.array([
        this.novoTelefone(),
        this.novoTelefone()
      ])
    }),
    beneficiarios: this._formBuilder.array([
      this.novoBeneficiario(true)
    ]),
    total: [null]
  });

  constructor(
    private _produtoService: ProdutosService,
    private _operadoraService: OperadoraService,
    private _vendedorService: VendedoresService,
    private _vendaService: VendaService,
    private _formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    private _route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this._operadoraService.getActive().subscribe(
      data => this.operadoras = data,
      error1 => {
        console.error(error1);
        this.snackBar.open('Erro ao carregar operadoras.', 'Fechar', {duration: 5000});
      }
    );

    this._vendedorService.getAtivos().subscribe(
      data => this.vendedores = data,
      error1 => {
        console.error(error1);
        this.snackBar.open('Erro ao carregar vendedores.', 'Fechar', {duration: 5000});
      }
    );

    this.adesao = this._route.snapshot.paramMap.get('adesao');
    if (this.adesao) {
      this._vendaService.getById(this.adesao).subscribe(
        data => {
          this.vendaSelecionada = data;
          this.vidas = data.produto.vidas;
          this.atualizarBeneficiaros(false);
          this.carregarProdutos(data.produto.operadora.id, data.produto.id);
        },
        err => {
          console.log(err);
          this.snackBar.open('Erro ao carregar dados.', 'Fechar', {duration: 5000,});
        },
        () => {
          this.vendaForm.patchValue(this.vendaSelecionada);
          this.atualizarTotal();

          const beneficiarios = this.vendaForm.get('beneficiarios') as FormArray;
          beneficiarios.controls
            .map(beneficiario => {
              beneficiario.patchValue({idade: this.calcIdade(beneficiario.get('data_nascimento').value, this.vendaSelecionada.data)});
            });
        }
      );
    }
  }

  carregarProdutos(operadora_id: number, produto_selecionado_id: number) {
    this.produtos = [];
    this._produtoService.getPorOperadora(operadora_id, true).subscribe(
      data => {
        this.produtos = data;
        if (produto_selecionado_id) {
          this.vendaForm.patchValue({produto: {id: produto_selecionado_id}, operadora: {id: operadora_id}});
        }
      },
      error1 => {
        console.log(error1);
        this.snackBar.open('Erro ao carregar produtos da operadora.', 'Fechar', {duration: 5000,});
      },
      () => {
        if (this.vendaSelecionada) {
          this.produtoSelecionado = this.produtos
            .filter(produto => produto.id === this.vendaSelecionada.produto.id).pop();
        }
      }
    );
  }

  atualizarProduto(produtoId: number) {
    this.produtoSelecionado = this.produtos.filter(produto => produto.id === produtoId).pop();
    if (!this.produtoSelecionado) {
      return;
    }
    this.vidas = this.produtoSelecionado.vidas;

    this.atualizarBeneficiaros(true);

  }

  adicionarDependente() {
    this.vidas++;
    this.atualizarBeneficiaros(true);
  }

  removerDependente() {
    this.vidas--;
    this.atualizarBeneficiaros(true);
  }

  atualizarBeneficiaros(atualizaPreco: boolean) {
    const beneficiarios = this.vendaForm.get('beneficiarios') as FormArray;
    while (this.vidas !== beneficiarios.length) {
      if (this.vidas < beneficiarios.length) {
        beneficiarios.removeAt(beneficiarios.length - 1);
      } else {
        beneficiarios.push(this.novoBeneficiario(false));
      }
    }

    this.adicionarDependenteHabilitado = (this.vidas > 3 && this.vidas < 6);
    this.removerDependenteHabilitado = (this.vidas > 4);

    if (atualizaPreco) {
      this.vendaForm.patchValue({total: 0});
      beneficiarios.controls
        .filter(beneficiario => beneficiario.get('data_nascimento').valid)
        .map(beneficiario => this.obterPreco(beneficiario as FormControl));
    }
  }


  novoTelefone(): FormGroup {
    return this._formBuilder.group({
      // id: [null],
      numero: [null]
    });
  }

  novoBeneficiario(ehTitular: boolean): FormGroup {
    return this._formBuilder.group({
      id: [null],
      nome: [null, Validators.required],
      cpf: [null, [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      data_nascimento: [null, Validators.required],
      preco: [null],
      idade: [null],
      e_titular: [ehTitular]
    });
  }

  calcIdade(dataNascimento: Date, hoje: Date): number {
    return moment(hoje).diff(dataNascimento, 'years');
  }


  onSubmit() {
    if (!this.vendaForm.valid) {
      this.vendaForm.markAsTouched();
      return;
    }

    const beneficiariosFormArray = this.vendaForm.get('beneficiarios') as FormArray;
    const beneficiarios = beneficiariosFormArray.controls.map(
      beneficiario => {
        return {
          cpf: beneficiario.get('cpf').value,
          nome: beneficiario.get('nome').value,
          data_nascimento: beneficiario.get('data_nascimento').value,
          preco: beneficiario.get('preco').value,
          e_titular: !!beneficiario.get('e_titular').value
        };
      });

    const telefonesFormArray = this.vendaForm.get('responsavel.telefones') as FormArray;
    const telefones = telefonesFormArray.controls
      .filter(telefone => telefone.get('numero').value && telefone.get('numero').value !== '')
      .map(
        telefone => {
          return {numero: telefone.get('numero').value};
        });

    // @ts-ignore
    const venda = {
      vendedor: {id: this.vendaForm.get('vendedor.id').value},
      produto: {id: this.vendaForm.get('produto.id').value},
      data: this.vendaForm.get('data').value,
      adesao: this.vendaForm.get('adesao').value,
      responsavel:
        {
          nome: this.vendaForm.get('responsavel.nome').value,
          cpf: this.vendaForm.get('responsavel.cpf').value,
          telefones: telefones
        },
      beneficiarios: beneficiarios,
      observacao: this.vendaForm.get('observacao').value,
      comissionada: true,
      taxa_adesao: 0
    };

    if (this.vendaSelecionada) {
      venda.adesao = this.vendaSelecionada.adesao;
      this._vendaService.put(venda).subscribe(
        () => {
          this.snackBar.open('Venda atualizada com sucesso.', 'Fechar', {duration: 3000,});
          this.vendaForm.reset({});
          this.vendaForm.clearValidators();
        },
        err => {
          console.log(err);
          this.snackBar.open('Erro ao atualizar venda.', 'Fechar', {duration: 5000,});
        }
      );
    } else {
      this._vendaService.post(venda).subscribe(
        () => {
          this.snackBar.open('Venda salva com sucesso.', 'Fechar', {duration: 3000,});
          this.vendaForm.reset({});
          this.vendaForm.clearValidators();
          this.vendaForm.clearAsyncValidators();
        },
        err => {
          console.log(err);
          this.snackBar.open('Erro ao salvar venda.', 'Fechar', {duration: 5000,});
        }
      );
    }
  }

  cancelar() {
    this._vendaService.dashboard();
  }

  obterPreco(beneficiarioFormControl: FormControl) {
    if (this.produtoSelecionado) {
      const idade = moment().diff(beneficiarioFormControl.get('data_nascimento').value, 'years');
      this._vendaService.getPreco(this.produtoSelecionado.id, idade).subscribe(
        data => {
          beneficiarioFormControl.patchValue({preco: data, idade: idade});
          this.atualizarTotal();
        },
        error1 => {
          this.snackBar.open('Erro ao obter preço.', 'Fechar', {duration: 5000,});
          console.log(error1);
        }
      );
    } else {
      this.snackBar.open('Não foi possível calcular o preço - selecione primeiro um produto.', 'Fechar', {duration: 5000,});
    }
  }

  atualizarTotal() {
    const beneficiariosFormArray = this.vendaForm.get('beneficiarios') as FormArray;
    this.vendaForm.patchValue({
      total: beneficiariosFormArray.controls
        .map(beneficiario => beneficiario.get('preco').value)
        .reduce((total, preco) => total + preco)
    });
  }


}
