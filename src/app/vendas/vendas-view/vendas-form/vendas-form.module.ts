import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VendasFormComponent} from './vendas-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatInputModule,
  MatSelectModule,
  MatSnackBarModule
} from '@angular/material';
import {NgxMaskModule} from 'ngx-mask';
import {NgxCurrencyModule} from 'ngx-currency';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatSelectModule,
    NgxMaskModule.forRoot({}),
    NgxCurrencyModule
  ],
  declarations: [VendasFormComponent],
  exports: [VendasFormComponent]

})
export class VendasFormModule {
}
