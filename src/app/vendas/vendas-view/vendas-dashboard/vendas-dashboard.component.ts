import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {VendaService} from '../../vendas-service/venda.service';
import {OperadoraService} from '../../../operadoras/operadoras-service/operadora.service';
import {VendedoresService} from '../../../vendedores/vendedores-service/vendedores.service';
import {Operadora} from '../../../operadoras/operadoras-model/operadora';
import {Vendedor} from '../../../vendedores/vendedores-model/vendedor';
import {VendaMin} from '../../vendas-model/venda';

@Component({
  selector: 'app-vendas-dashboard',
  templateUrl: './vendas-dashboard.component.html',
  styleUrls: ['./vendas-dashboard.component.css']
})
export class VendasDashboardComponent implements OnInit {

  hoje = new Date();
  data_inicio = new Date();
  columnsToDisplay = ['operadora', 'produto', 'vidas', 'data', 'adesao', 'titular', 'valor', 'editar'];

  buscaVendaForm: FormGroup;
  operadoras: Operadora[];
  vendedores: Vendedor[];

  dataSource: MatTableDataSource<VendaMin>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    private _vendaService: VendaService,
    private _operadoraService: OperadoraService,
    private _vendedorService: VendedoresService,
  ) {
  }

  ngOnInit() {
    this.data_inicio.setDate(this.data_inicio.getDate() - 7);

    this.buscaVendaForm = this._formBuilder.group({
      adesao: [null],
      data_inicio: [this.data_inicio, Validators.required],
      data_fim: [this.hoje, Validators.required],
      operadora_id: [null],
      vendedor_id: [null]
    });

    this._operadoraService.getActive().subscribe(
      data => this.operadoras = data,
      error1 => {
        console.error(error1);
        this.snackBar.open('Erro ao carregar operadoras.', 'Fechar', {duration: 5000});
      }
    );

    this._vendedorService.getAtivos().subscribe(
      data => this.vendedores = data,
      error1 => {
        console.error(error1);
        this.snackBar.open('Erro ao carregar vendedores.', 'Fechar', {duration: 5000});
      }
    );
  }

  onSubmit() {
    if (this.buscaVendaForm.get('data_inicio').value > this.buscaVendaForm.get('data_fim').value) {
      this.snackBar.open('Favor informar data de ínicio menor que data final.', 'Fechar', {duration: 5000});
    } else {
      this._vendaService.get(this.buscaVendaForm.value).subscribe(
        dados => {
          this.dataSource = new MatTableDataSource(dados);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

          if (this.dataSource.data.length < 1) {
            this.snackBar.open('Nenhuma venda encontrada.', 'Fechar', {duration: 3000});
          }
        },
        error1 => {
          this.snackBar.open('Erro ao buscar vendas.', 'Fechar', {duration: 5000});
          console.log(error1);
        }
      );
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  novo() {
    this._vendaService.novo();
  }

  editar(id: number) {
    this._vendaService.editar(id);
  }
}
