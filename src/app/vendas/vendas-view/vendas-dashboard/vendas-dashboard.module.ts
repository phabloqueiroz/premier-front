import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VendasDashboardComponent} from './vendas-dashboard.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
  ],
  declarations: [VendasDashboardComponent],
  exports: [VendasDashboardComponent]
})
export class VendasDashboardModule {
}
